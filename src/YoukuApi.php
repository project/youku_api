<?php

/**
 * The Youku Api class, which provides all the youku integrations.
 *
 * Extends this class, if you want to customize some of the behavior
 */
class YoukuApi {

  const BASE_URL = 'https://openapi.youku.com/v2/';

  /**
   * The active configuration used in API calls.
   *
   * @var array
   */
  protected $config;

  /** Internal functions */

  /**
   * Constructs a new YoukuApi instance.
   *
   * @param array $config
   *   The configuration
   */
  public function __construct(array $config = array()) {
    $default = array(
      'client_id' => variable_get('youku_api_client_id', ''),
      'client_secret' => variable_get('youku_api_client_secret', ''),
    );
    $this->config = array_merge($config, $default);

    // Initialize the cURL handle.
    $this->ch = curl_init();
    $this->setDefaultCurlOptions();
  }

  /**
   * Closes the cURL handle when the object is destroyed.
   */
  public function __destruct() {
    if (is_resource($this->ch)) {
      curl_close($this->ch);
    }
  }

  /** Static functions */

  /**
   * Get the video id from an Youku URL or embed code.
   *
   * @param string $url
   *   The Youku url / embed to decode for the video id.
   *
   * @return string|null
   *   The video id or null if no video id could be extracted
   */
  public static function getVideoId($url) {
    $patterns = array(
      '@v\.youku\.com/v_show/id_([^"\&/]+)\.html@i',
      '@player\.youku\.com/player\.php/sid/([^"\&/]+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $url, $matches);
      if (isset($matches[1])) {
        return ($matches[1]);
      }
    }
    return NULL;
  }

  /** Publlic functions */

  public function videoShow($video_id) {
    return $this->doRequest('videos/show', array('video_id' => $video_id));
  }

  /** Protected functions */

  /**
   * Performs a request.
   *
   * @param $api
   *   The api to use.
   * @param $params
   *   An array of params to include with the request. Optional.
   * @param $method
   *   The HTTP method to use. One of: 'GET', 'POST', 'PUT', 'DELETE'.
   *
   * @return array
   *   An array with the 'success' boolean and the result. If 'success' is FALSE
   *   the result will be an error message. Otherwise it will be an array
   *   of returned data.
   */
  protected function doRequest($api, array $params = array(), $method = 'GET') {
    // Add the client id if it's missing, as it's needed for all requests.
    if (empty($params['client_id'])) {
      $params['client_id'] = $this->config['client_id'];
    }

    $url = self::BASE_URL . $api . '.json';

    if ($method == 'GET') {
      $url = url($url, array('query' => $params));
    }
    elseif ($method == 'POST') {
      $params = json_encode($params);
      curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);
    }

    curl_setopt($this->ch, CURLOPT_URL, $url);
    curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);

    $success = FALSE;
    $result = curl_exec($this->ch);
    $status_code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
    if ($status_code != '200') {
      $result = 'Error ' . $status_code;
    }
    elseif ($status_code == '200' && !empty($result)) {
      $result = json_decode($result, TRUE);
      $success = TRUE;
    }

    return array('success' => $success, 'result' => $result);
  }

  /**
   * Sets the default cURL options.
   */
  protected function setDefaultCurlOptions() {
    $header = array(
      'Content-Type: application/json; charset=utf-8',
    );

    curl_setopt($this->ch, CURLOPT_HEADER, FALSE);
    curl_setopt($this->ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    curl_setopt($this->ch, CURLOPT_VERBOSE, FALSE);

    curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($this->ch, CURLOPT_TIMEOUT, 180);
  }

  /** Getters and setters */

  /**
   * Gets the active config.
   *
   * @return array
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Sets the active config.
   *
   * @param array $config
   *
   * @return $this
   */
  public function setConfig($config) {
    $this->config = $config;
    return $this;
  }

}
