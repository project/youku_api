<?php

/**
 * @file
 * Admin forms and callbacks for youku api module.
 */

/**
 * General settings form.
 */
function youku_api_settings_form($form, &$form_state) {
  $form['youku_api_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client id'),
    '#description' => t('Enter the client id for the youku app you want to use.'),
    '#default_value' => variable_get('youku_api_client_id', ''),
  );

  $form['youku_api_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#description' => t('Enter the secret key associated with the client id, needed for some API requests.'),
    '#default_value' => variable_get('youku_api_client_secret', ''),
  );

  return system_settings_form($form);
}
